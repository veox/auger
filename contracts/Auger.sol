pragma solidity ^0.4.11;

contract Auger {
    /// @dev magicnum
    uint constant BITSPERBYTE = 8;
    /// @dev magicnum: len(bytes32) - len(address) == 32 - 20
    uint constant OFFSET = 12;
    /// @dev magicnum: all bits set to 1
    bytes32 constant FWORD = 0xffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff;

    /// @dev Extended log.
    struct prospector {
        uint blocknum;
        address caller;
        address coinbase;
        uint gaslimit;
        uint gaspercall;
        uint gasperturn;
    }
    mapping (bytes32 => prospector) public prospectors;

    // TODO: What should be logged? What of that should be indexed?
    event logDrilled(address indexed caller, bytes32 indexed input, bytes32 indexed output);
    event logGasUsesUpdated(address indexed caller, uint indexed gaspercall, uint indexed gasperturn);

    /// @note Non-loop gas usage.
    uint public gaspercall;
    /// @note One iteration's gas usage.
    uint public gasperturn;

    /**
       @dev To be run when gas uses change, e.g. after a fork.
    */
    function updateGasUses() public {
        uint gasbefore;
        uint gasafter;
        uint gasdiff;
        bytes32 sample0;
        bytes32 sample1;

        uint oldgaspercall = gaspercall;
        uint oldgasperturn = gasperturn;

        // measure how much gas use is just paperwork...
        gasbefore = msg.gas;
        sample0 = drill({turns: 0, randomdata: FWORD});
        gasafter = msg.gas;
        // ... by comparing gas reserves before/after
        gasdiff = gasbefore - gasafter;
        gaspercall = gasdiff;

        // measure how much a turn actually costs - do it...
        gasbefore = msg.gas;
        sample1 = drill({turns: 1, randomdata: FWORD});
        gasafter = msg.gas;
        // ... and subtract the cost of paperwork
        gasdiff = gasbefore - gasafter;
        gasperturn = gasdiff - gaspercall;

        // don't store if nothing changed
        if (gaspercall == oldgaspercall) {
            delete prospectors[sample0];
        }
        if (gasperturn == oldgasperturn) {
            delete prospectors[sample1];
        }

        logGasUsesUpdated({caller: msg.sender, gaspercall: gaspercall, gasperturn: gasperturn});
    }

    /**
       @dev Constructor - immediately determine gas use.
    */
    function Auger() {
        updateGasUses();
    }

    /**
       @dev Modify storage and emit event.
    */
    function _record(bytes32 input, bytes32 result, uint turns) private {
        prospector p = prospectors[result];
        p.blocknum = block.number;
        p.caller = msg.sender;
        p.coinbase = block.coinbase;
        p.gaslimit = block.gaslimit;
        p.gaspercall = gaspercall;
        p.gasperturn = gasperturn;

        logDrilled({caller: msg.sender, input: input, output: result});
    }

    /**
       @dev Convenience - no data provided, use FWORD. >:(
    */
    function drillLazy(uint turns) external returns(bytes32) {
        // XORing `x` with FWORD will produce bitwise negation of `x`
        return drill({turns: turns, randomdata: FWORD});
    }
    /**
       @dev Some data provided, hopefully random - use that to burn gas, and log fact.
    */
    function drill(uint turns, bytes32 randomdata) public returns(bytes32) {
        bytes32 data = bytes32(msg.sender) << (BITSPERBYTE * OFFSET);
        data ^= bytes32(block.coinbase);
        data ^= randomdata;

        bytes32 ret = doDrill({data: data, caller: msg.sender, turns: turns});

        _record({turns: turns, input: randomdata, result: ret});
        return ret;
    }

    /**
       @dev Burn gas, don't log fact.
    */
    function doDrill(bytes32 data, address caller, uint turns) public constant returns(bytes32) {
        uint256 nbytes;

        for (uint256 i = 0; i < turns; i++) {
            // will shift this many bytes
            nbytes = uint256(data) % OFFSET;
            // XOR with sender, shifted left between 0 and OFFSET bytes
            data ^= bytes32(caller) << nbytes * BITSPERBYTE;
            // XOR with Keccak-256 hash of self
            data ^= sha3(data);
        }

        return data;
    }

    /**
       @notice Fallback - not allowed.
     */
    function () {
        throw;
    }
}
