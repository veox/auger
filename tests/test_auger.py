def test_auger(chain):
    auger, _ = chain.provider.get_or_deploy_contract('Auger')

    assert auger is not None
