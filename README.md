# Auger

Ethereum contract to "request" one-off block gas limit increase.

Send a transaction to this with more gas than the current gas limit.

Solidity and Populus.

Why the name? See [drilling rig][rig] and [auger][auger] referenced
there.

[rig]: https://en.wikipedia.org/wiki/Drilling_rig
[auger]: https://en.wikipedia.org/wiki/Auger_(drill)


## License

GPLv3. See `LICENSE.txt`.
